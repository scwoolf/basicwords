require 'test_helper'

class RootPageControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get root_page_home_url
    assert_response :success
  end

end
