class ApplicationController < ActionController::Base
   
   def permitted?
       session[:permission]
   end
   
   def lives
      session[:lives]
   end
   
   def permit
      session[:permission] = true
   end
   
   def unpermit
      session[:permission] = false
   end
    
   def generate_session
       session[:username] = SecureRandom.base64(12)
       session[:lives] = 3
       session[:score] = 0
   end
   
    def no_session?
       session[:username].nil?
    end
   
   def end_session
       idx = Game.where(username: session[:username]).map {|el| el.id}
       Game.destroy(idx)
       session.delete(:username)
       session[:score] = 0
   end
   
   def decrement_word
       Game.create(username: session[:username], word_id: session[:current_word])
       session[:score] += 1
   end
   
   def decrement_lives
      session[:lives] -= 1
   end
   
   def select_word
      all_words = (1..Word.all.size).to_a
      correct_words = Game.where(username: session[:username]).map {|w| w.word_id}
      remaining_words = all_words - correct_words
      session[:current_word] = remaining_words[rand(remaining_words.size)]
   end
   
   def current_word
       Word.find_by_id(session[:current_word])
   end
    
end
