class Api::V1::FinalScoreController < ApplicationController
    
    def generate
        @finalscore = session[:score]
        @lastplace = TopTen.minimum(:score)
        end_session
    end
    
    def add_to_table
        TopTen.create({name: params[:name], score: params[:score]})
        TopTen.destroy(TopTen.find_by_score(TopTen.minimum(:score)).id);
    end
    
    
    
end
