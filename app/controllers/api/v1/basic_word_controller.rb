class Api::V1::BasicWordController < ApplicationController
    attr_accessor :current_lives
    
    def generate
        
        if no_session?
            generate_session
            select_word
            permit
        end
        
         @word = current_word.term
         @score = session[:score]
         
        if permitted?
            @answers = current_word.generate_answers
            @lives = lives
            @flag = permitted?
            unpermit
        else  
            if lives > 0
                decrement_lives
                @answers = [current_word.definition]
                @lives = lives
                @flag = !permitted?
            else
                @flag = permitted?
            end
        end
        
        render :generate

    end
    
    def evaluate
        
        if (lives > 0) && (current_word.correct?(params[:answer]) || (params[:answer] == ""))
            decrement_word unless params[:answer] == ""
            select_word
            permit unless session[:score] == Word.all.size 
        end
        
        redirect_to api_v1_basicword_path
        
    end    

end
