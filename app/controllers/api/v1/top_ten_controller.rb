class Api::V1::TopTenController < ApplicationController
    
    def index
        @topten = TopTen.all.order(score: :desc)
        render json: @topten
    end
    
end
