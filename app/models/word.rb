class Word < ApplicationRecord
    
    validates :term, :part_of_speech, :definition, presence: true

    
    def generate_answers
        answers = []
        answers << definition
        selection = Word.where(part_of_speech: part_of_speech)
        while answers.size < 4 do
            answers << selection[rand(selection.size)].definition
            answers = answers.uniq
        end
        answers = answers.shuffle
    end
    
    def correct?(ans)
        definition == ans ? true : false
    end
    
end
