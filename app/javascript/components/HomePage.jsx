import React from "react";
import { Link } from "react-router-dom";

export default () => (
  
<div className="container-fluid vh-100 justify-content-center d-flex align-items-center bg-dark">
<div className="container mw-50">
  <h1 className="display-2 text-light text-center">basic words</h1>
  <div className="row justify-content-center">
  <Link to="/basicword" className="btn btn-light btn-lg mr-3" role="button">PLAY</Link>
  <Link to="/topten" className="btn btn-light btn-lg" role="button">TOP TEN</Link>
  </div>
  </div>
</div>
  
  
);