import React from 'react';
import { Link } from 'react-router-dom';

class TopTen extends React.Component {
  
constructor(props){
  super(props);
  this.state = { topten: [] };
}
  
componentDidMount(){
  const url = 'api/v1/topten';
  
  fetch(url)
    .then(response => {
      if (response.ok){
        return response.json();
      }
      throw new Error("Network response was not ok");
    })
    .then(response => this.setState({topten: response}))
    .catch(() => this.props.history.push("/"));
}

render(){
  
  const { topten } = this.state;
  
  const TopTenList = topten.map((winner,index) => (
    
    <tr key={index + 1}>
      <th className="text-center" scope="row">{index + 1}</th>
      <td className="text-center" >{winner.name}</td>
      <td className="text-center" >{winner.score}</td>
    </tr>
    ));
  
  return(
      
    <div className="container-fluid bg-dark d-flex align-items-center min-vh-100 w-100 py-3">
    <div className="container-fluid">
    <div className="container w-50 bg-light pt-3 pb-1 mx-auto">
    <table className="table w-90 table-dark mx-auto">
  <thead>
    <tr>
      <th className="text-center" scope="col">#</th>
      <th className="text-center" scope="col">Name</th>
      <th className="text-center" scope="col">Score</th>
    </tr>
  </thead>
  <tbody> 
    {TopTenList}
  </tbody>
</table>
</div>
<div className="row justify-content-center">
  <Link to="/basicword" className="btn btn-light btn-lg mt-3 mr-3" role="button">PLAY</Link>
  <Link to="/" className="btn btn-light btn-lg mt-3" role="button">QUIT</Link>
</div>
</div>
</div>
    );
}
}

export default TopTen;