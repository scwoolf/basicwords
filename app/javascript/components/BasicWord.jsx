import React from 'react';
import { Link, Redirect } from 'react-router-dom';

class BasicWord extends React.Component {
  
constructor(props){
  super(props);
  this.state = { word: "", answers: [], lives: 3, score: 0, flag: true};
  this.onClick = this.onClick.bind(this);
}

componentDidMount(){
  const url = 'api/v1/basicword';
  
  fetch(url)
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      throw new Error("Network response was not ok.");
    })
    .then(response => this.setState({ word: response.word, answers: response.answers, lives: response.lives, score: response.score, flag: response.flag }))
    .catch(() => this.props.history.push("/basicword"));
}

onClick(e){
   e.preventDefault();
   const url = 'api/v1/basicword';
   const params = {answer: e.target.value};
   const token = document.querySelector('meta[name="csrf-token"]').content;
    fetch(url, {
      method: "POST",
      headers: {
        "X-CSRF-Token": token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify(params)
    })
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      throw new Error("Network response was not ok.");
    })
    .then(response => this.setState({ word: response.word, answers: response.answers, lives: response.lives, score: response.score, flag: response.flag }))
    .catch(() => this.props.history.push("/basicword"));
    
}

render(){
    
    const { word, answers, lives, score, flag } = this.state;  
    
    if (flag === true){
      let answerList = [];
    
    if (answers.length > 1){
      
    answerList = answers.map((answer,index)=>(
        <button onClick={this.onClick} className="btn btn-light btn-lg btn-block" role="button" key={index+1} value={answer}>{answer}</button>
        ));
        
    } else {
      
    answerList = answers.map((answer,index)=>(
        <button onClick={this.onClick} className="btn btn-danger btn-lg btn-block" role="button" key={index+1} value="">CORRECT ANSWER: {answer}</button>
        ));
    }
    
    let hearts = [];
    for (let i = 0; i < lives; i++){
      hearts.push(<svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="30px" height="40px" viewBox="153 198 306 396">
<path fill="pink" stroke="#000000" strokeMiterlimit="10" d="M307.5,518.4c-0.5-0.4-0.9-0.7-1.2-1c-13.1-14.2-27.9-26.6-43.2-38.3  c-14.8-11.3-29.8-22.4-44.6-33.6c-12.9-9.8-25-20.5-35.8-32.6c-8.8-9.8-16-20.5-20.6-33c-4.6-12.6-6.4-25.6-4.9-38.9  c2.3-20.1,10.9-37.1,26.9-49.9c9-7.2,19.3-11.5,30.4-14.1c17.7-4.1,34.8-2.2,51.3,5.1c14.1,6.3,24.6,16.7,32.6,29.8  c2.5,4.2,4.6,8.6,6.9,12.9c0.3,0.5,0.5,1,1,1.7c0.5-1,0.9-1.9,1.3-2.7c6.3-14,15.3-25.8,27.9-34.8c10.3-7.4,21.7-11.7,34.3-13.3  c9.7-1.2,19.4-1.1,28.9,1.2c23.5,5.6,49,21.8,55.9,55.2c2,9.7,2.4,19.5,1.2,29.4c-0.7,6-2.8,11.8-4.9,17.5  c-6.1,16.2-15.8,29.9-27.7,42.2c-9.3,9.5-19.5,17.8-30,25.8c-14.8,11.4-29.7,22.5-44.4,34c-12.8,10-25.2,20.5-36.4,32.3  C310.7,514.8,309.2,516.6,307.5,518.4z"/>
</svg>);
    }
        
           
 return( 
      <div className="container-fluid justify-content-center bg-dark">
      <div className="d-flex justify-content-between">
      <h3 className="container-fluid text-light text-left pt-3 pr-3">lives: {hearts}</h3>
      <h3 className="container-fluid text-light text-right pt-3 pr-3">score: {score}</h3>
      </div>
      <div className="d-flex align-items-center vh-100">
      <div className="container w-50">
       <h1 className="display-2 text-light text-center">{word}</h1>
      <div>{answerList}</div>
      </div>
      </div>
      </div>
      );
    } else {
      
      return(
        <Redirect to="/finalscore" />
        );
        
    }
}
}

export default BasicWord;


