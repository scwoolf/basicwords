import React from "react";
import { Link } from "react-router-dom";

class FinalScore extends React.Component {
  
  constructor(props){
    super(props);
    this.state = {finalscore: "", lastplace: "", winner: ""};
    this.onChange = this.onChange.bind(this);
    this.onClick = this.onClick.bind(this);
  }
  
  componentDidMount(){
  const url = "api/v1/finalscore";
  
  fetch(url)
    .then(response => {
      if (response.ok){
        return response.json();
      }
      throw new Error("Network response was not ok");
    })
    .then(response => this.setState({finalscore: response.finalscore, lastplace: response.lastplace}))
    .catch(() => this.props.history.push("/finalscore"));
}

onChange(e){
  e.preventDefault();
  this.setState({winner: e.target.value});
}

onClick(e){
   e.preventDefault();
   const url = 'api/v1/finalscore';
   const params = {name: this.state.winner, score: this.state.finalscore};
   const token = document.querySelector('meta[name="csrf-token"]').content;
    fetch(url, {
      method: "POST",
      headers: {
        "X-CSRF-Token": token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify(params)
    })
    .then(() => this.props.history.push("/topten"))
    .catch(() => this.props.history.push("/finalscore"));
    
}
  
  render(){
    
    const { finalscore, lastplace, winner } = this.state;
    
    const toptenbutton = ( <div className="row justify-content-center">
                <h3 className="text-light text-center">Congrats! You've made the Top Ten!</h3>
                <form className="w-100 mb-3" onSubmit={this.onClick}>
                <div className="input-group">
                <input type="text" className="form-control" placeholder="Name" value={this.state.winner} onChange={this.onChange}/>
                  <div className="input-group-append">
                  <button className="btn btn-light" type="submit">SUBMIT</button>
                  </div>
                </div>
                  </form>
                </div>
                );
                
              const standard = (
              <div className="row justify-content-center">
              <Link to="/basicword" className="btn btn-light btn-lg mr-3" role="button">PLAY</Link>
              <Link to="/topten" className="btn btn-light btn-lg" role="button">TOP TEN</Link>
              </div>);

    return(
  
<div className="container-fluid vh-100 justify-content-center d-flex align-items-center bg-dark">
<div className="container-fluid w-50">
  <h1 className="display-2 text-light text-center">final score: {finalscore}</h1>
  
  {finalscore > lastplace ? toptenbutton : standard}

  </div>
</div>
);
}
}
  
export default FinalScore;