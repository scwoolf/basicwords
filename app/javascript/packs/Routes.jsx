import 'bootstrap/dist/css/bootstrap.min.css';
import $ from 'jquery';
import Popper from 'popper.js';
import 'bootstrap/dist/js/bootstrap.bundle.min';

import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter, Switch, Route} from 'react-router-dom'

import HomePage from '../components/HomePage'
import TopTen from '../components/TopTen'
import BasicWord from '../components/BasicWord'
import FinalScore from '../components/FinalScore'

class Routes extends React.Component {
  render(){
    return( <>{
      <BrowserRouter>
      <Switch>
      <Route path='/topten' exact component={TopTen} />
      <Route path='/basicword' exact component={BasicWord} />
      <Route path='/finalscore' exact component={FinalScore} />
      <Route path='/' exact component={HomePage} />
      </Switch>
      </BrowserRouter>
    }</>
  );
}
}

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <Routes/>,
    document.body.appendChild(document.createElement('div')),
  )
})
