# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'csv'
file = IO.read('./lib/assets/basicwords.json')
words = JSON.parse(file)

words.each do |w|
  Word.create(term: w["term"], part_of_speech: w["part_of_speech"], definition: w["definition"])
end

TopTen.create(name:"Cynthia",score: 100)
TopTen.create(name:"Hunter",score: 90)
TopTen.create(name:"Miguel",score: 80)
TopTen.create(name:"Fatima",score: 70)
TopTen.create(name:"Jalen",score: 60)
TopTen.create(name:"Connie",score: 50)
TopTen.create(name:"Tran",score: 40)
TopTen.create(name:"Arjun",score: 30)
TopTen.create(name:"Taylor",score: 20)
TopTen.create(name:"Jade",score: 10)