class CreateWords < ActiveRecord::Migration[6.0]
  def change
    create_table :words do |t|
      t.string :term
      t.string :part_of_speech
      t.string :definition

      t.timestamps
    end
  end
end
