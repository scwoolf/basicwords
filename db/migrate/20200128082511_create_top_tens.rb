class CreateTopTens < ActiveRecord::Migration[6.0]
  def change
    create_table :top_tens do |t|
      t.string :name
      t.integer :score

      t.timestamps
    end
  end
end
