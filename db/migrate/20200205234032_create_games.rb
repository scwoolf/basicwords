class CreateGames < ActiveRecord::Migration[6.0]
  def change
    create_table :games do |t|
      t.string :username
      t.references :word, null: false, foreign_key: true

      t.timestamps
    end
  end
end
