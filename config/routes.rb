Rails.application.routes.draw do
  root 'root_page#home'
  namespace :api do
    namespace :v1 do
      get '/topten', to: 'top_ten#index'
      get '/basicword', to: 'basic_word#generate', defaults: {format: :json}
      post '/basicword', to: 'basic_word#evaluate', defaults: {format: :json}
      get '/finalscore', to: 'final_score#generate', defaults: {format: :json}
      post '/finalscore', to: 'final_score#add_to_table'
    end
  end
  get '/*path', to: 'root_page#home'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
